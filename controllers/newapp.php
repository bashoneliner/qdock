<?php

include('../core/JamDockProject.php');
include('../core/JamDockFunctions.php');
include('../core/error.php');

//container names must be unique
foreach($_POST['container'] as $container){
	//make sure it isn't in the database or repeated in the array
	if(containerExists($container)){
		error(" Error: container name already exists",1);
	}
	$checkDups=array_count_values($_POST['container']);
	if($checkDups[$container] > 1){
		error(" Error: Duplicate container name in form",1);
	}
}

//initialize a new app
$app_name=$_POST['app_name'];
$app=new JamDockProject;
$app_id=$app->newApp($app_name);
if(! $app_id){
	error(" Error:app_id",1);
}

//handle creating all of the user-defined directories
if(is_array($_POST['directory'])){
	$directory=$_POST['directory'];
}else{
	$directory=array();
}
foreach($directory as $dir){
	if(!empty($dir)){
		$app->addDir($dir);
	}
}

//get the list of containers that we are creating
if(is_array($_POST['container'])){
	$containers=array();
	foreach($_POST['container'] as $index=>$container){
		if(!empty($container)){
			$containers[$index]=$container;
		}
	}
}

//make sure that the index of each container name in the array matches the index of its corresponding template in its array
if(is_array($_POST['template']) && is_array($_POST['container'])){
	$templates=array();
	foreach($_POST['template'] as $index=>$template){
		if(!empty($template)){
			//get the actual ID from the template
			$result=$app->db->query("Select id from templates WHERE name='$template' LIMIT 1");
			if($result){
				$template_id=$result->fetch_row();
				$template_id=$template_id[0];
				$templates[$index]=$template_id;
			}

		}
	}

	$containers=array();
	foreach($_POST['container'] as $index=>$container){
		if(!empty($container)){
			$containers[$index]=$container;
		}
	}
}


//add the container name and template to the database
foreach($containers as $index=>$container){
	if(!templateExists($templates[$index])){
		echo "Template does not exist";
		continue;
	}
	$app->db->query("Insert into containers values('id','$container',$app->id,$templates[$index])");
}

