#!/bin/bash

TAG=$1

docker pull jamersan/${TAG}_wp
docker pull jamersan/${TAG}_mysql
docker pull jamersan/${TAG}_data

#set up the directories
mkdir /root/docker/$TAG
cd /root/docker/$TAG
cp -r /root/docker/resources/* /root/docker/$TAG

#make a directory for logs. We will need them.
mkdir -p $(pwd)/logs/apache2
mkdir -p $(pwd)/configs/htpasswd

docker run \
$DAEMON $GET_TERM  \
$LOGS \
-e MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD} \
-e MYSQL_USER=${MYSQL_USER} \
-e MYSQL_DATABASE=${MYDB} \
-e MYSQL_PASSWORD=${MYSQL_PASSWORD} \
--volumes-from ${TAG}_data \
--name $MYSQLTAG \
$MYSQLTAG \
$DOCKER_CMD

echo "mysql host = $(docker inspect $MYSQLTAG|grep '"IPAddress"'|egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}')" >>./my.info

#build apache
docker run $DAEMON $GET_TERM -p $PORT:80 -v $(pwd)/src/wordpress:/var/www/html $HTPASSWD $LOGS --link $MYSQLTAG --name $WPTAG $WPTAG $DOCKER_CMD

echo "$(cat <<EOF
	Your WordPress install is finished. Below are your MySQL credentials. They are also saved to /root/docker/$TAG/my.info.
	$(cat ./my.info)
EOF
)"


