#!/bin/bash

docker login
docker tag ${TAG}_wp jamersan/${TAG}_wp
docker tag ${TAG}_mysql jamersan/${TAG}_mysql
docker tag ${TAG}_data jamersan/${TAG}_data

docker push jamersan/${TAG}_wp
docker push jamersan/${TAG}_mysql
docker push jamersan/${TAG}_data


