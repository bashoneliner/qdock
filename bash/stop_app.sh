#!/bin/bash

TAG=$1

if docker ps|grep -q ${TAG}_wp; then
	docker stop ${TAG}_wp
	docker stop ${TAG}_mysql
	docker stop ${TAG}_data
	docker rm ${TAG}_wp
fi


