#!/bin/bash

DAEMON='-d'

function USAGE(){
	echo  -e "\nbuild_mysql -p [port] -t [tag name] -d [db name] -u [mysql user] -P [mysql password] [options]\n"
	echo "-d        Daemonize the container"
	echo "-p [port] Host port for accessing MySQL"
	echo "-t [name] Tag name for the container"
	echo "-D        Debug mode. This starts the container with bash and attaches to its pty"
	echo "-l        Mount /var/log/mysql to $(pwd)/logs/mysql after creating the directory if it does not exist"
	echo "-u [user] Name of the mysql user"
	echo "-P [pass] Password for mysql user"
	echo -e "-d [db]   Name of the database to use/create\n"
}

function error(){
	echo $2
	USAGE
	exit 1
}

while getopts ":d:t:DSlu:P:m:" opts; do
    case "$opts" in
        d)
	  DAEMON='-d'
	  ;;

	t)
	  TAG=${OPTARG}
	  ;;

	D)
	  DEBUG='true'
	  DOCKER_CMD='/bin/bash'
	  GET_TERM='-ti'
	  DAEMON=''
	  ;;

	l)
	  LOGS="-v $(pwd)/logs/mysql/:/var/log/mysql/"
	  ;;

	u)
	  MYUSER="${OPTARG}"
	  ;;

	P)
	  MYPASS="${OPTARG}"
	  ;;

	m)
	  MYDB="${OPTARG}"
	  ;;

        *)
            USAGE
            ;;
    esac
done

[ -z "$TAG" ] && error "You must enter a tag name with -t"
[ -z "$MYUSER" ] && error "You must enter the name for the MySQL user with -u"
[ -z "$MYDB" ] && MYDB="$MYUSER"

#make a directory for logs. We will need them.
mkdir -p $(pwd)/logs/mysql/

MYSQL_ROOT_PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | sed 1q)
[ -z "$MYSQL_PASSWORD" ] && MYSQL_PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | sed 1q)

echo "$(cat <<EOF
mysql root password = $MYSQL_ROOT_PASSWORD
mysql user = $MYUSER
mysql user password = $MYSQL_PASSWORD
mysql database name = $MYDB
EOF
)" >my.info

docker pull jamersan/dev-mysql_base
docker tag jamersan/dev-mysql_base $TAG
docker create --name ${TAG}_data -v /var/lib/mysql mysql

docker run \
$DAEMON $GET_TERM  \
$LOGS \
-e MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD} \
-e MYSQL_USER=${MYUSER} \
-e MYSQL_DATABASE=${MYDB} \
-e MYSQL_PASSWORD=${MYSQL_PASSWORD} \
--volumes-from ${TAG}_data \
--name $TAG \
$TAG \
$DOCKER_CMD
