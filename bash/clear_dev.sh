#!/bin/bash

TAG=$1

docker rm -f ${TAG}_wp ${TAG}_mysql ${TAG}_data
docker rmi ${TAG}_wp ${TAG}_mysql ${TAG}_data

