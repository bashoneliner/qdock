#!/bin/bash

function error(){
	echo $1
	exit
}

DAEMON='-d'

while getopts "d:p:t:DSl" opts; do
    case "$opts" in
        d)
	  DAEMON='-d'
	  ;;

	p)
	  PORT=${OPTARG}
	  ;;

	t)
	  TAG=${OPTARG}
	  ;;

	D)
	  DEBUG='true'
	  DOCKER_CMD='/bin/bash'
	  GET_TERM='-ti'
	  DAEMON=''
	  ;;

	S)
	  SECURE='true'
	  ;;
	l)
	  LOGS="-v $(pwd)/logs/apache2/:/var/log/apache2/"
	  ;;
        *)
            usage
            ;;
    esac
done

[ -z "$TAG" ] && error "You must enter a tag name with -t"
[ -z "$PORT" ] && error "Your must enter an external port number with -p"

#make a directory for logs. We will need them.
mkdir -p $(pwd)/logs/apache2

function secure(){
	#make the directory structure if it doesn't exist
	mkdir -p $(pwd)/configs/htpasswd

	#Create the first user
	echo "Creating a new user for htpasswd"
	echo "Please enter a user name"
	read USER
	while [ -z "$USER" ]; do
		echo "You must enter a user name"
	done

	htpasswd -c $(pwd)/configs/htpasswd/htpasswd $USER

	#Create users as long as we don't say no
	echo "Enter another username to add more users. Leave empty and press ENTER when done"
	read USER
	while [[ ! -z "$USER" ]]; do
		htpasswd $(pwd)/configs/htpasswd/htpasswd $USER
		echo "Enter another username to add more users. Leave empty and press ENTER when done"
		read USER
	done

	#Secure the site using htaccess
	cat $(pwd)/configs/.htaccess >$(pwd)/src/wordpress/.htaccess
	HTPASSWD="-v $(pwd)/configs/htpasswd/:/var/www/htpasswd/"
}

#get WP source and place it in a directory on the host that we will bind mount so we can make code changes outside of the container
function getWP(){
	cd src
	rm -rf src/*
	curl https://wordpress.org/latest.tar.gz|tar --overwrite -zx
	cd ..
}

#Get WP files
getWP

#If -S is passed then prompt for a user and password for htpasswd
[ "$SECURE" = 'true' ] && secure

docker pull jamersan/dev-apache_base:latest
docker tag jamersan/dev-apache_base $TAG
docker run $DAEMON $GET_TERM -p $PORT:80 -v $(pwd)/src/wordpress:/var/www/html $HTPASSWD $LOGS --name $TAG $TAG $DOCKER_CMD


