
$(function(){

	//Use this to make sure all of our dynamically inserted fields get labeled
	function labelIt(selector){
		$(selector).labelify({labelledClass: "labelText"});
	}
	
	//label all of the non dynamically created inputs
	labelIt(":text");

	//add another input field for directories each time the button is clicked
	$("#add_directory_field").button({
		icons:{
			primary: "ui-icon-plusthick"
		},
		text: false
	}).click(function(){
		var $newDirectoryField=$('<input type="text" name="directory[]" title="Add a directory to your project">');
		$newDirectoryField.addClass('labelText').addClass('directories').appendTo("#directory_fields");
		labelIt("#directory_fields input[type=text]");
		return false;
	});


	//add input fields for container and template each time the button is clicked
	var i=3;
	$("#add_container_field").button({
		icons:{
			primary: "ui-icon-plusthick"
		},
		text: false
	}).click(function(){
		var $newContainerField=$('<span class="container_template_wrapper"><input type="text" name="container[]" title="Container name" class="container_field labelText" id="container_'+i+'"><input type="text" name="template[]" class="template_field labelText" id="template_'+i+'" title="Specify a template"></span>');
		$newContainerField.addClass('labelText').addClass('container_filed').appendTo("#container_fields");
		labelIt("#container_fields input[type=text]");
		i++;
		return false;
	});

	//don't allow duplicate container names
	$("#app_name_field").on('keyup', function(){
		$.get("/JamDockFunctions.php?function=appExists&name="+$(this).val(),function(data){
			if(data == 'true'){
				$("#app_name_field").css('background-color','#FFA8C1');
				$("#app_name_warning").html("Name in use ");
			}else{
				$("#app_name_field").css("background-color","");
				$("#app_name_warning").html("");
			}
		});
	});

	//don't allow duplicate container names
	$("#container_fields").on('keyup', '.container_field', function(){
		var field=event.target.id;
		$.get("/JamDockFunctions.php?function=containerExistsR&name="+$(this).val(),function(data){
			if(data == 'true'){
				$('#'+field).css('background','#FFA8C1');
				$('#send_form').attr('disabled','disabled');
			}else{
				$('#'+field).css('background','');
				$('#send_form').removeAttr('disabled');
			}
		});
	});

	//make the submit button pretty
	$("#send_form").button();

	//make the template fields auto-complete
	$('#container_fields').on('keyup', ".template_field", function(){
		if($(this).val() != ''){
			$(this).autocomplete({
				source: "/JamDockFunctions.php?function=returnTemplateNames&string="+$(this).val()

			});
		}
	});

});
