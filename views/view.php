<?php

?>

<head>
	<link rel="stylesheet" href="../js/jquery-ui/jquery-ui.min.css">
	<link rel="stylesheet" href="../style/labelify.css">
	<link rel="stylesheet" href="../style/newapp.css">
	<script src="../js/jquery-ui/external/jquery/jquery.js"></script>
	<script src="../js/jquery-ui/jquery-ui.min.js"></script>
	<script src="../js/jamdock/newapp.js"></script>
	<script src="../js/jamdock/labelify.js"></script>
</head>

<body>
	<span id="content">
		<form action="../controller.php" method="POST">
			<span id="form_wrapper">
				<span id="name_span">
					<!--// Begin App name  //-->
					<span id="app_name_warning"></span>
					<input type="text" name="app_name" title="App Name" id="app_name_field"><br>

				</span>
				<span id="directory_span">
					<!--//Inputs for creating user-defined directories //-->
					<button id="add_directory_field"></button>
					<span id="directory_fields">
						 <input type="text" name="directory[]" title="Add a directory to your project" class="directories">
					</span><br>
				</span><br>

				<!--//inputs for adding containers to the app //-->
				<span id="container_span">
					<button id="add_container_field"></button>
					<span id="container_fields">
						<span class="container_template_wrapper">
							<input type="text" name="container[0]" id="container_1" title="Container name" class="container_field" ><input type="text" name="template[0]" id="template_1" class="template_field" title="Specify a template">
						</span>
					</span>
				</span>
			</span>
			<br><br><br><button id="send_form">Save</button>
		</form>
	</span>
</body>
