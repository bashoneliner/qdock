<?php


//This is where any sources that we download or want to reuse will be stored
define('APP_SRC_DIR','/home/mathew.moon/html/jamdock/sources');

//This is where projects will be stored
define('PROJECTS_DIR','/home/mathew.moon/html/jamdock/projects');

//This is where configs that can be reused will be stored
define('CONFIGS_DIR', '/home/mathew.moon/html/jamdock/configs');

//Mysql password
define('MYPWD','password');

//mysql user
define('MYUSER','user');

//Database name
define('MYDB','database');

//Machine that MySQL is running on
define('DATABASE_HOST','localhost');
