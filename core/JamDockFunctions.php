<?php
require_once('settings.php');

function dbConnect(){
	$db=new mysqli(DATABASE_HOST, MYUSER, MYPWD, MYDB) or die("cannot connect to database");
	return $db;
}

if(!empty($_GET['function'])){
	$function=$_GET['function'];
	$function();
}

function fake(){
	return false;
}

function listImages(){
	$CMD="docker images|egrep -v 'REPOSITORY\s*TAG\s*IMAGE\sID\s*CREATED\s*VIRTUAL\sSIZE|^<none>'";
	$images=array();
	exec($CMD,$output);
	foreach($output as $line){
		$line=preg_split('/\s{2,}/',$line);
		$images[$line[2]]=array('name'=>$line[0],'tag'=>$line[1],'hash'=>$line[2],'date'=>$line[3],'size'=>$line[4]);;
	}
	return $images;
}

function inspectContainer($name){
	$CMD="docker inspect $name";
	exec($CMD, $out, $val);
	if($val > 0 ){
		return false;
	}else{
		return $out;
	}
}

function returnTemplates(){
	$db=dbConnect();
	$string=$_GET['string'];
	if(empty($_GET['by'])){
		$by='name';
	}else{
		$by=$_GET['by'];
	}

	$result=$db->query("Select * from templates where $by LIKE '".$string."%' ");
	if($result){
		while($row=$result->fetch_assoc()){
			$results[]=json_encode($row);
		}
		print_r(json_encode($results));
	}
}

function returnTemplateNames(){
	$db=dbConnect();
	$string=$db->escape_string($_GET['string']);
	$result=$db->query("Select name from templates where name LIKE '".$string."%'");
	if($result){
		while($row=$result->fetch_assoc()){
			$names[]=($row['name']);
		}
		print_r(json_encode($names));
	}
}

function templateExists($id){
	$db=dbConnect();
	if(mysqli_num_rows($db->query("Select id from templates where id=$id")) != 0){
		return true;
	}else{
		return false;
	}
}

function containerExists($name){
	$db=dbConnect();
	if(mysqli_num_rows($db->query("Select id from containers where name='$name'")) != 0){
		return true;
	}else{
		return false;
	}
}

function appExists(){
	$db=dbConnect();
	$name=$_GET['name'];
	if(mysqli_num_rows($db->query("Select id from app where name='$name'")) != 0){
		echo 'true';
		return true;
	}else{
		echo 'false';
		return false;
	}
}

function containerExistsR(){
	$db=dbConnect();
	$name=$_GET['name'];
	if(mysqli_num_rows($db->query("Select id from containers where tag='$name'")) != 0){
		echo 'true';
		return true;
	}else{
		echo 'false';
		return false;
	}
}

function listAllowedImages(){
	$db=dbConnect();
	$string=$_GET['string'];
	$result=$db->query("Select image from images where image LIKE '{$string}%'");
	if($result){
		while($row = $result->fetch_assoc()){
			$images[]=$row['image'];
		}
		print_r(json_encode($images));
	}
}

function listConfigs(){
	$db=dbConnect();
	$string=$db->escape_string($_GET['string']);
	$result=$db->query("Select file_name from configuration_files where file_name LIKE '".$string."%'");
	if($result){
		while($row = $result->fetch_assoc()){
			$configs[]=$row['file_name'];
		}
		print_r(json_encode($configs));
	}
}

function templateNameExists($name){
	$db=dbConnect();
	if($db->query("Select * from templates where name='$name'")->num_rows > 0){
		return true;
	}else{
		return false;
	}
}

function configNameExists($name){
	$db=dbConnect();
	if($db->query("Select * from configuration_files where file_name='$name'")->num_rows > 0){
		return true;
	}else{
		return false;
	}
}

