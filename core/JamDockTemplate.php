<?php

include('JamDock.php');

class jamDockTemplate extends jamDock{
	protected $templateId;
	protected $name;
	protected $description;
	protected $image;

	public function jamDockTemplate(){
		require_once('settings.php');
		$this->dbConnect();
	}

	//create a new template entry and return the id
	public function addTemplate($name,$description,$image){
		$this->name=$name;
		$this->description=$description;
		$this->image=$image;

		if(empty($this->image) || empty($this->description) || empty($this->name)){
			return false;
		}

		if($this->db->query("Insert into templates values('id','$this->name','$this->description','$this->image')")){
			$this->templateId=$this->db->insert_id;
			return $this->templateId;
		}
		else{
			echo $this->db->error;
			return false;
		}
	}

	/**
	*   Add config file to the template by creating a map between the configuration_files.id templates.id including where the file will be bind mounted to.
	*   addConfig(100,10,'/etc/apache2/apache.conf') will create a map that takes uses the configuration with 'id' of 100 and add it to the template with the id of 10.
	*   The $location variable will let the template know where in the filesystem on the container to mount the file to
	**/
	public function addConfig($config_id,$template_id,$location){
		$result=$this->db->query("Select file_name from configuration_files where id=$config_id");
		$file_name=$result->fetch_assoc();
		$file_name=$file_name['file_name'];
		if(! $file_name){
			return false;
		}

		//create the file map
		if($this->db->query("Insert into config_template_map values('id','$config_id','$this->templateId','$location')")){
			return $this->db->insert_id;
		}else{
			return false;
		}
	}

	//add directories that we need to create in the project directory
	public function addDirs($template_id,$template_id){
		if($this->db->query("Insert into directories values('id','$dir_name','$template_id')")){
			return $this->db->insert_id;
		}else{
			return false;
		}
	}

	//add environment variables to a template
	public function addEnv($key,$val,$template_id){
		if($this->db->query("Insert into env values('id','$key','$val','$template_id')")){
			return $this->db->insert_id;
		}else{
			return false;
		}
	}

	//mount a local directory to a directory on the host
	public function addBindMount($host_point,$container_point,$template_id){
		if($this->db->query("Insert into bind_mounts values('id','$host_point','$container_point','$template_id')")){
			return $this->db->insert_id;
		}else{
			return false;
		}
	}


	//add port mappings to the container template
	public function addPorts($host_port,$container_port,$template_id){
		if($this->db->query("Insert into ports values('id',$host_port,$container_port,$template_id)")){
			return $this->db->insert_id;
		}else{
			return false;
		}
	}


	//add volumes for external persistent storage
	public function addVolumeMap($template_id,$volume_id){
		if($this->db->insert_id("Insert into volume_template_map values('id',$template_id,$volume_id)")){
			return $this->db-insert_id;
		}else{
			return false;
		}
	}

	//return all of the various parts of a template
	public function returnTemplate($id){

		//get the basic template info
		$result=$this->db->query("Select * from templates where id='$id'");
		$info=$result->fetch_assoc();
		$template['info']=$info;

		//get all of the environment vars
		$envs=array();
		$result=$this->db->query("Select id, CONCAT_WS('=',var,value) as variable from env where template_id=$id");
		if($result){
			while($row=$result->fetch_assoc()){
				$envs[$row['id']]=$row['variable'];
			}
		}

		$template['Environment Variables']=$envs;

		//get all of the bind mounts
		$mounts=array();
		$result=$this->db->query("Select id,CONCAT_WS(':',host_point,container_point) as mount from bind_mounts where template_id=$id");
		if($result){
			while($row=$result->fetch_assoc()){
				$mounts[$row['id']]=$row['mount'];
			}
		}
		$template['Bind Mounts']=$mounts;

		//get all of the configuration files
		$configs=array();
		$result=$this->db->query("Select cm.id, cm.configuration_files_id, cm.mount_to, cf.file_name, cf.body from config_template_map cm inner join configuration_files cf on cm.configuration_files_id=cf.id where cm.template_id=$id");
		if($result){
			while($row=$result->fetch_assoc()){
				$configs[$row['id']]=$row['file_name'].':'.$row['mount_to'];
			}
		}
		$template['Confings']=$configs;

		//get all of the directories that we need to create
		$directories=array();
		$result=$this->db->query("Select dir_name from directories where template_id=$id");
		if($result){
			while($row=$result->fetch_array()){
				$directories[]=$row['dir_name'];
			}
		}

		$template['Directories']=$directories;

		//get all of the port mappings
		$ports=array();
		$result=$this->db->query("Select host_port,container_port from ports where template_id=$id");
		if($result){
			while($row=$result->fetch_array()){
				$ports[]=$row['host_port'].':'.$row['container_port'];
			}
		}

		$template['Ports']=$ports;

		//Get all of the external volumes that we are using
		$volumes=array();
//		$result=$this->db->query("Select id,volume_id from volume_template_map where template_id=$id");
		$result=$this->db->query("Select v.name, m.volume_id From volumes v inner join volume_template_map m on m.volume_id=v.id where m.template_id=$id");
		if($result){
			while($row=$result->fetch_array()){
				$volumes[]=$row['name'];
			}
		}

		$template['Volumes']=$volumes;

		//Return the array
		return $template;
	}
}

//$template=new JamDockTemplate;
//print_r($template->returnTemplate(10));
//$app=new jamDockTemplate('first template','my first template');
//echo $app->addBindMount('logs/apache2/','/var/log/apache2',10)." \n";
//$app->newTemplate();
//$app->listOfImages=array('apache'=>'dev-apache_base', 'mysql'=>'dev-mysql_base');
//$app->htpasswd=true;
//$app->webserverPort=8088;
//$app->tag='wp';
//$app->src='wordpress';
//$app->htusers=array('mathew'=>'mathew','test'=>12345,'test2'=>12345);
//$app->git='https://mathewmoon:MMmm012783@bitbucket.org/mathewmoon/testrepo.git';
//$app->src='wordpress';
//print_r($app->newApp());

