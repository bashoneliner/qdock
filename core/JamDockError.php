<?php
error_reporting(E_ERROR | E_PARSE | E_NOTICE);

function error($message,$fatal=0){
	$debug=true;
	if(! is_int($fatal)){
		die('You messed up your error call. $fatal should be an integer');
	}

	if($fatal == 1 && $debug == true){
		die("\n".'Error:'."$message"."\n");
	}
}
