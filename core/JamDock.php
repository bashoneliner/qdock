<?php
require_once('error.php');
/**
*	A class for creating an application consisting of one or more docker containers
*
*
*
*
**/

class JamDock{
	private $git_out;
	private $mysqlEnvStr;
	private $mysqlEnv;
	private $projectSQL;
	private $mysqlVolume;
	private $mysqlCMD;
	private $mysqlImage;
	private $webServerCMD;
	private $mysqlTag;
	private $webserverTag;
	private $configDir;
	private $webserverImage;
	private $sql_container=false;
	private $htaccess;

	public $mysqlPort;
	public $htpasswd=false;
	public $webserverPort;
	public $git;
	public $mysql_user;
	public $mysql_database;
	public $mysql_password;
	public $secure;
	public $action;
	public $src;
	public $listOfImages;
	public $myPwd;
	public $myRootPwd;
	public $myUser;
	public $myDatabase;
	public $dev;
	public $containerCommands;

	protected $db;
	protected $projectSrc;
	protected $projectDir;
	protected $tag;

	public function JamDock(){
		require_once('settings.php');
		$this->mysqlImage='jamersan/dev-mysql_base';
		$this->webserverImage='jamersan/dev-apache_base';
	}

	public function newAppp(){
		if(! preg_match('/^[a-zA-Z0-9-_]/',$this->tag)){
			error("Tags must follow the format of '^[a-zA-Z0-9_-]*$'",1);
			return false;
		}

		if(! empty($this->src) && ! empty($this->git)){
			error("Select either a src name or git repo, but not both.", 1);
			return false;
		}
		elseif(empty($this->src) && empty($this->git)){
			error("You must have either a git repo or existing source app.",1);
			return false;
		}

		if(empty($this->src)){
			$this->src=APP_SRC_DIR.'/'.$this->tag;
		}
		else{
			$this->src=APP_SRC_DIR.'/'.$this->src;
		}

		$this->projectDir=PROJECTS_DIR.'/'.$this->tag;
		$this->projectSrc=$this->projectDir.'/src';
		$this->projectSQL=$this->projectDir.'/sql';
		$this->configDir=$this->projectDir.'/configs';

		if(is_dir($this->projectDir)){
			error("Project $this->tag already exists.",1);
			return false;
		}
		else{
			exec("mkdir -p $this->projectSrc");
		}

		$this->getGit();
		$this->getSrc();

		if(array_key_exists('mariadb',$this->listOfImages) || array_key_exists('mysql',$this->listOfImages)){
			$this->mysqlContainer=true;
			$this->mysqlEnv=$this->mysqlVars();
			$this->buildMysqlCMD();
		}

		if(array_key_exists('apache',$this->listOfImages) || arrary_key_exists('nginx', $this->listOfImages)){
			$this->buildWebServerCMD();
			$this->webServerContainer=true;
		}

		$this->buildContainers();
		$this->mysqlEnv['host']=system('docker inspect '.$this->mysqlTag.' |grep IPAddress|egrep -o "([0-9]{1,3}\.){3}[0-9]{1,3}"');
		return array('Project Name'=>$this->tag, 'Project Directory'=>$this->projectDir, 'Project Source Directory'=>$this->projectSrc, 'Git Repo'=>$this->git, 'Git output'=>$this->git_out,'Mysql environment variables'=>$this->mysqlEnv,'Mysql Environment String'=>$this->mysqlEnvStr,'Container build output'=>$this->containerBuildOut,'Container Build Commands'=>$this->containerCommands);
	}

	private function getGit(){
		if(! empty($this->git)){
			//make the directory
			exec("mkdir -p ".$this->src, $mkdir_out, $mkdir_val);

			//error if we return a non 0 exit conde
			if($mkdir_val != 0){
				error("Cannot create directory from source from $this->git",1);
				return false;
			}

			//clone from git
			exec('git clone '.$this->git.' '.$this->src.' 2>&1', $this->git_out, $git_val);

			//error if we return a non 0 exit conde
			if($git_val != 0){
				error("Cannot clone from $this-git",1);
				return false;
			}

			return $this->src;
		}
	}

	private function getSrc(){
		//make the project's source directory
		if(! is_dir($this->projectSrc)){
			exec("mkdir -p ".$this->projectSrc);
		}

		//see if we have the source in a directory
		if( ! is_dir($this->src)){
			error("The application source that you specified cannot be found.", 1);
			return false;
		}
		else{
			//copy it to the project if we do
			exec('cp -r '.$this->src.'/* '.$this->projectSrc, $cp_out, $cp_val);
			if($cp_val != 0){
				error("Error copying source from $this->src to $this->projectSrc",1);
				return false;
			}
			return $this->projectSrc;
		}
	}


	private function mysqlVars(){
		//Set the mysql user password if not already done
		if(empty($this->myPwd)){
			$this->myPwd=str_shuffle(md5(rand(1000000000000000,9999999999999999)));
		}
		//set the mysql root password if not already done
		if(empty($this->myRootPwd)){
			$this->myRootPwd=str_shuffle(md5(rand(1000000000000000,9999999999999999)));
		}

		//Set the database name to the tag if it is empty
		if(empty($this->myDatabase)){
			$this->myDatabase=$this->tag;
		}

		//Set the mysql user name
		if(empty($this->myUser)){
			$this->myUser=$this->tag;
		}

		$this->mysqlEnvStr="-e MYSQL_PASSWORD=$this->myPwd -e MYSQL_ROOT_PASSWORD=$this->myRootPwd -e MYSQL_DATABASE=$this->myDatabase -e MYSQL_USER=$this->myUser";
		return array('myPwd'=>$this->myPwd,'myRootPwd'=>$this->myRootPwd,'myDatabase'=>$this->myDatabase,'myUser'=>$this->myUser);
	}

	private function buildMysqlCMD(){
		if(! empty($this->mysqlPort)){
			$this->mysqlPort="-p $this->mysqlPort";
		}

		$this->mysqlTag=$this->tag.'_mysql';
		$this->mysqlVolume=$this->mysqlTag.'_data';

		$this->mysqlCMD="docker run -d --name $this->mysqlTag $this->mysqlPort $this->mysqlEnvStr -v $this->projectSQL".":/docker-entrypoint-initdb.d/ --volumes-from $this->mysqlVolume $this->mysqlImage 2>&1";
		$this->containerCommands[]=$this->mysqlCMD;

		return $this->mysqlCMD;
	}

	private function buildWebServerCMD(){
		//set the external port that the webserver will listen on
		$this->webserverPort=$this->webserverPort.':80';

		//define what image we are going to use

		$this->webserverTag=$this->tag.'_web';
		//$this->webserverImage=$this->listOfImages['web'];
		if($this->htpasswd == true){
			$this->htpasswdMount='-v '.$this->configDir.'/htpasswd:/var/www/htpasswd';
			$defaultHtaccess=file_get_contents(CONFIGS_DIR.'/htaccess');
			$createHtaccess=system("touch $this->projectSrc".'/.htaccess');
			$ht=fopen($this->projectSrc.'/.htaccess','w+');
			fwrite($ht, $defaultHtaccess);
			fclose($ht);
		}

		//the command that will launch the server
		$this->webServerCMD="docker run -d --name $this->webserverTag -p $this->webserverPort -v $this->projectSrc".":/var/www/html/  $this->htpasswdMount  --link $this->mysqlTag $this->webserverImage 2>&1";
		$this->containerCommands[]=$this->webServerCMD;

	}

	private function buildContainers(){
		if($this->mysqlContainer == true){
			//someplace to stash db dumps
			exec("mkdir $this->projectSQL");

			//create the data volume that mysql will use
			exec("docker create --name $this->mysqlVolume -v /var/lib/mysql mysql 2>&1", $mysql_data_output, $mysql_data_val);

			//build the actual mysql container
			exec($this->mysqlCMD,$mysql_output,$mysql_val);
		}

		if($this->webServerContainer == true){
			//create the htpasswd file and add users if we are using it
			if($this->htpasswd == true){
				exec("mkdir -p $this->configDir"."/htpasswd/ && touch $this->configDir".'/htpasswd/htpasswd');
				foreach($this->htusers as $user=>$password){
					exec("htpasswd -b $this->configDir"."/htpasswd/htpasswd $user $password",$htpasswd_out,$htpasswd_val);
				}
			}

			//build the web server container
			exec($this->webServerCMD,$webserver_cmd_output,$webserver_cmd_val);
		}

		$this->containerBuildOut=array('Webserver Build messages'=>$webserver_cmd_output,'Mysql Build messages'=>$mysql_output, 'Mysql Data container messages'=>$mysql_data_output);
		return array($this->containerBuildOut);

	}

	protected function dbConnect(){
			$this->db=new mysqli(DATABASE_HOST, MYUSER, MYPWD, MYDB) or die("cannot connect to database");
//			$result=$this->db->query("Select * from env");
//			while($row=mysqli_fetch_assoc($result)){
//				print_r($row);
//			}
	}


}

