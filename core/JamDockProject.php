<?php
//error_reporting(E_ERROR | E_PARSE | E_NOTICE);
//include('JamDock.php');
include('JamDockError.php');

class JamDockProject{

	public function JamDockProject(){
		require_once('./settings.php');
		$this->dbConnect();
	}

	public function newApp($name,$directories){
		//A little validation goes a long way
		if(! preg_match('/^[a-zA-Z0-9-_]/',$name)){
			error("Tags must follow the format of '^[a-zA-Z0-9_-]*$'",1);
			return false;
		}

		//Create the project in the database
		$name=$this->db->real_escape_string($name);

		if($this->db->query("Insert into app values('id','$name')")){
			$this->tag=$name;
			$this->id=$this->db->insert_id;
		}else{
			error(" Error: cannot insert app_name into db");
			return false;
		}

		//create the directories that we will need
		//base directory for the project
		$this->projectDir=PROJECTS_DIR.'/'.$this->tag;
		if(! is_dir($this->projectDir)){
			mkdir($this->projectDir,0755,true);
		}else{
			error(" Project $this->tag already exists");
		}

		//this is a place to store source code
		$this->projectSrc=$this->projectDir.'/src';
		mkdir($this->projectSrc,0755,true);

		//a place to store sql dumps
		$this->projectSQL=$this->projectDir.'/sql';
		mkdir($this->projectSQL,0755,true);

		//a place to store config directories that will be bind mounted
		$this->configDir=$this->projectDir.'/configs';
		mkdir($this->configDir,0755,true);

		return $this->id;

}
/*

	        //create all of the user defined directories
		foreach($directories as $directory){
			$this->addDir($directory,$this->id);
		}

		return $this->id;
	}



	public function addSrc(){
		//We will require either git or existing code locally
		if(! empty($this->src) && ! empty($this->git)){
			error("Select either a src name or git repo, but not both.", 1);
			return false;
		}elseif(empty($this->src) && empty($this->git)){
			error("You must have either a git repo or existing source app.",1);
			return false;
		}

		//copy over the source from the destination
		if(is_dir(APP_SRC_DIR.'/'.$this->src) && ! empty($this->src)){
			system("cp -r ".APP_SRC_DIR.'/'.$this->src.'/* '.$this->projectSrc.'/');
			return $this->src;
		}else{
			return false;
		}


		//clone from git if we are using that
		if(! empty($this->git)){
			//clone from git
			exec('git clone '.$this->git.' '.$this->src.' 2>&1', $this->git_out, $git_val);

			//error if we return a non 0 exit conde
			if($git_val != 0){
				error("Cannot clone from $this-git",1);
				return false;
			}

			return $this->src;
		}
	}

*/
	//make the userdefined directories
	public function addDir($dirname){
		if(empty($dirname)){
			return false;
		}
		if($this->db->query("Insert into directories values('id','$dirname',$this->id)")){
			mkdir($this->projectDir.'/'.$dirname,0755,true);
			return $this->db->insert_id;
		}else{
			error(" Error: Could not insert directory name into database",1);
			return false;
		}
	}

        protected function dbConnect(){
                        $this->db=new mysqli(DATABASE_HOST, MYUSER, MYPWD, MYDB) or die("cannot connect to database");
        }
}

