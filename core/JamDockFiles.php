<?php

include('JamDock.php');

class JamDockFile extends JamDock{

	public function JamDockFile(){
		require_once('./settings.php');
		$this->dbConnect();
	}

	//stores a new config into the database
	public function saveConfig($filename,$body){
		$body=$this->db->real_escape_string($body);
		if($this->db->query("Insert into configuration_files values('id','$filename','$body')")){
			return $this->db->insert_id;
		}
		else{
			return false;
		}
	}

	//deletes a config
	public function deleteConfig($id){
		if($this->db->query("Delete from configuration_files where id=$id LIMIT 1")){
			return true;
		}
		else{
			return false;
		}
	}

	//updates a config
	public function updateConfig($id,$body){
		$body=$this->db->real_escape_string($body);
		if($this->db->query("Update configuration_files set body='$body' Where id=$id LIMIT 1")){
			return true;
		}
		else{
			return false;
		}
	}

}

$file=new JamDockFile;
$file->updateConfig(3,'this has changed');
