<?php

include('JamDockTemplate.php');
include('JamDockFunctions.php');

class jamDockBuild extends jamDockTemplate{

	protected $template=array();

	public function jamDockBuild(){
		require_once('./settings.php');
		$this->dbConnect();
	}

	//Creates the actual docker command that will be used to spin up the container from a template stored in the database
	public function buildContainer($template_id){

		//lets make sure that the template exists and get all of the info if we do.
		$result=$this->db->query("Select * from templates where id=$template_id");
		if($result){
			$template=$result->fetch_assoc();

			//state the name, description, and image used for the template.
			$this->name=preg_replace('/\s{1,}/','_', $template['name']);
			$this->image=$template['image'];
			$this->description=$template['description'];
		}

		//Now we need to retrieve the template
		$this->template=$this->returnTemplate($template_id);

		//Create the bind mount options
		$mounts='-v '.implode(' -v ',$this->template['Bind Mounts']);

		//Create the port mapping
		$port='-p '.implode(' -v',$this->template['Ports']);

		//grab the environment variables
		$envs='-e '.implode(' -e ', $this->template['Environment Variables']);

		//get any volumes that we need to mount
		$volumes='--volumes-from '.implode(' --volumes-from', $this->template['Volumes']);

		$CMD="docker run -d --rm $mounts $port $envs $volumes --name $this->name $this->image";
		return $CMD;

	}

	//Create an actual container for storing data
	public function newVolume($image,$name){
		//Stop if the container already exists
		if(inspectContainer($name)){
			die('Already there');
			return false;
		}

		if(preg_match('/\s/',$name)){
			$name=preg_replace('/\s/','_',$name);
		}

		$CMD="docker run -d --name $name $image";
		exec($CMD, $output);
		return $output;
	}
}

$build=new JamDockBuild;
//echo $build->buildContainer(10)."\n\n\n";
print_r($build->newVolume('jamersan/mysql_data','test data volume'));

